from exporterutilities.DbMannager import DbMannager
from datetime import *

sprints_query_by_gaf = "select * from unity_schema.sprints where isactive = true and sprintid in " \
                 "(select sprintid from unity_schema.stories where projectid = " \
                 "(select jiraprojectid from unity_schema.development_projects where gafid = {}))"

units_query = "select * from unity_schema.units"

flights_query = "select * from unity_schema.gafs where unitid = {}"

teams_query = "select * from unity_schema.teams where gafid = {}"

features_stories_query = "select * from unity_schema.stories where IssueType in ('Task','Story') and projectid = " \
                 "(select jiraprojectid from unity_schema.development_projects where gafid = {})"

bugs_stories_query = "select * from unity_schema.stories where IssueType = 'Bug' and projectid = " \
                 "(select jiraprojectid from unity_schema.development_projects where gafid = {})"

get_all_cycles_id_by_project_id_query = "SELECT * FROM unity_schema.cycles where projectid = {} "

get_all_status_of_execution_tests_by_cycle_id = "SELECT executionstatus, COUNT(*)" \
                        "FROM unity_schema.execution_tests where cycleid = {} GROUP BY executionstatus"

get_all_plans_by_gaf_id_query = "select * from unity_schema.bamboo_plans where projectkey = " \
                        "(select bambooprojectid from unity_schema.development_projects where gafid = {})"

get_all_builds_by_plan_query = "select * from unity_schema.bamboo_builds where plankey = '{}' " \
                               "order by creationtime desc limit 8"

pull_last_build_status_by_plan_key_query = "select ispassed from unity_schema.bamboo_builds where plankey = '{}'" \
                                           "order by creationtime desc limit 1"

get_open_pull_requests_by_repo_id_query = "select * from unity_schema.bitbucket_pull_requests where repoid = {}"

get_all_repos_by_gaf_id = "select * from unity_schema.bitbucket_repos where projectid = " \
                        "(select BitBucketProjectID from unity_schema.development_projects where gafid = {})"

get_commits_of_last_closed_pull_request_by_repo_id = "select commitmessage, pullrequestid " \
                                                     "from unity_schema.bitbucket_commits " \
                                                     "where pullrequestid = (select pullrequestid " \
                                                     "from unity_schema.bitbucket_pull_requests " \
                                                     "where isopen = false and tobranchname = 'master' " \
                                                     "order by creationtime desc limit 1) and repoid = {}"

get_pull_request_date_by_id_query = "select creationtime from unity_schema.bitbucket_pull_requests " \
                                    "where pullrequestid = {}"

get_developer_by_id_query = "select name from unity_schema.developers where developerid = '{}'"

get_work_reports_query = "select * from unity_schema.work_reports where storyid in " \
                   "(select storyid from unity_schema.stories where projectid = " \
                   "(select jiraprojectid from unity_schema.development_projects where gafid = {}))" \
                   "OR taskid in (select taskid from unity_schema.stories where projectid = " \
                   "(select jiraprojectid from unity_schema.development_projects where gafid = {}))"

get_developer_by_work_report_query = "select * from unity_schema.developers where developerid = " \
                           "(select reporterid from unity_schema.work_reports where storyid = '{}')"

get_team_by_developer_id_query = "select * from unity_schema.teams where teamid = " \
                         "(select teamid from unity_schema.developers where developerid = '{}')"

get_developer_by_team_id_query = "select * from unity_schema.developers where teamid = {}"

get_developer_work_report_by_id_and_date_query = "select * from unity_schema.work_reports " \
                                 "where reporterid = '{}' and datereported = '{}'"


class Repository:
    def __init__(self):
        self.db = DbMannager()

    def sprints_story(self, flight_id):
        return self.db.postgressql_select_queries(sprints_query_by_gaf.format(flight_id))

    def pull_units(self):
        return self.db.postgressql_select_queries(units_query)

    def pull_gaf_by_unit(self, unit_id):
        return self.db.postgressql_select_queries(flights_query.format(unit_id))

    def pull_teams_by_gaf(self, gaf_id):
        return self.db.postgressql_select_queries(teams_query.format(gaf_id))

    def pull_features_stories_by_gaf(self, gaf_id):
        return self.db.postgressql_select_queries(features_stories_query.format(gaf_id))

    def pull_bugs_stories_by_gaf(self, gaf_id):
        return self.db.postgressql_select_queries(bugs_stories_query.format(gaf_id))

    def pull_plans_by_gaf(self, gaf_id):
        return self.db.postgressql_select_queries(get_all_plans_by_gaf_id_query.format(gaf_id))

    def pull_builds_by_plan_key(self, plan_key):
        return self.db.postgressql_select_queries(get_all_builds_by_plan_query.format(plan_key))

    def pull_last_build_status(self, plan_key):
        return self.db.postgressql_select_queries(pull_last_build_status_by_plan_key_query.format(plan_key))

    def pull_repos_by_gaf_id(self, gaf_id):
        return self.db.postgressql_select_queries(get_all_repos_by_gaf_id.format(gaf_id))

    def pull_pull_requests_by_repo_id(self, repo_id):
        return self.db.postgressql_select_queries(get_open_pull_requests_by_repo_id_query.format(repo_id))

    def pull_all_commits_of_last_merged_pull_request_by_repo_id(self, repo_id):
        return self.db.postgressql_select_queries(get_commits_of_last_closed_pull_request_by_repo_id.format(repo_id))

    def pull_pull_request_date_by_id(self, pull_request_id):
        return self.db.postgressql_select_queries(get_pull_request_date_by_id_query.format(pull_request_id))

    def developer_name_by_id(self, developer_id):
        return self.db.postgressql_select_queries(get_developer_by_id_query.format(developer_id))

    def work_report_by_gaf_id(self, gaf_id):
        return self.db.postgressql_select_queries(get_work_reports_query.format(gaf_id, gaf_id))
    
    def get_developer_by_work_report_id_query(self, story_id):
        return self.db.postgressql_select_queries(get_developer_by_work_report_query.format(story_id))
    
    def get_team_by_developer(self, developer_id):
        return self.db.postgressql_select_queries(get_team_by_developer_id_query.format(developer_id))

    def pull_team_members_by_team_id(self, team_id):
        return self.db.postgressql_select_queries(get_developer_by_team_id_query.format(team_id))

    def pull_relevant_work_report_by_developer_id_and_report_date(self, developer_id, report_date):
        return self.db.postgressql_select_queries(
            get_developer_work_report_by_id_and_date_query.format(developer_id, report_date))
        
    def pull_cycles_id_by_project_id(self, project_id):
        return self.db.postgressql_select_queries(get_all_cycles_id_by_project_id_query.format(project_id))

    def pull_all_status_execution_tests_status_by_cycle_id(self, cycle_id):
        return self.db.postgressql_select_queries(get_all_status_of_execution_tests_by_cycle_id.format(cycle_id))


# if __name__ == '__main__':
#     rep = Repository()
#     aaa = rep.pull_team_members_by_team_id(1)
#     for row in aaa:
#         print(row)


