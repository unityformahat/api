import json
import pprint
from Models.Sprint import *
from Repository.Repository import Repository
from datetime import *


repository = Repository()
sprint = Sprint()


class Service:

    def __init__(self):
        pass

    def critical_bugs_pull(self, gaf_id):
        returned_json = []
        all_bugs = repository.pull_bugs_stories_by_gaf(gaf_id)
        current_bug_data = {}
        for bug in all_bugs:
            if bug[10] == "Highest":
                current_bug_data["bug_id"] = bug[0]
                current_bug_data["bug_name"] = bug[1]
                returned_json.append(current_bug_data)
                current_bug_data = {}
        return json.dumps(returned_json)

    def pull_sprint_data(self, gaf_id):
        returned_json = []
        current_sprint_data = {}
        data = repository.sprints_story(gaf_id)
        for active_sprint in data:
            current_sprint_data["sprint_id"] = active_sprint[0]
            current_sprint_data["sprint_name"] = active_sprint[1]
            current_sprint_data["sprint_remaining_time"] = sprint.calculate_remaining_sprint_time(active_sprint[3])
            current_sprint_data["sprint_passed_time_percent"] = \
                sprint.calculate_passed_sprint_time_percent(active_sprint[2], active_sprint[3])
            returned_json.append(current_sprint_data)
            current_sprint_data = {}
        return json.dumps(returned_json)

    def pull_hierarchy_data(self):
        returned_json = {"units": []}
        units = repository.pull_units()
        unit_json = {}
        for unit in units:
            unit_json["unit_name"] = unit[1]
            unit_json["unit_id"] = unit[0]
            unit_json["gafs"] = []
            gafs = repository.pull_gaf_by_unit(unit[0])
            gaf_json = {}
            for gaf in gafs:
                gaf_json["gaf_name"] = gaf[1]
                gaf_json["gaf_id"] = gaf[0]
                gaf_json["teams"] = []
                teams = Repository().pull_teams_by_gaf(gaf[0])
                for team in teams:
                    gaf_json["teams"].append(team[1])
                unit_json["gafs"].append(gaf_json)
                gaf_json = {}
            returned_json["units"].append(unit_json)
            unit_json = {}

        return json.dumps(returned_json)

    def pull_open_pull_requests(self, gaf_id):
        returned_json = []
        repos = repository.pull_repos_by_gaf_id(gaf_id)
        current_repo = {}
        for repo in repos:
            current_repo["repoid"] = repo[0]
            current_repo["reponame"] = repo[1]
            current_repo["open_pull_requests_to_master"] = []
            current_pull_request = {}
            pull_requests = repository.pull_pull_requests_by_repo_id(repo[0])
            for pull_request in pull_requests:
                if pull_request[4] and pull_request[6] == "master":
                    current_pull_request["create_date"] = str(pull_request[2])
                    current_pull_request["developername"] = repository.developer_name_by_id(pull_request[3])
                    current_pull_request["frombranchname"] = pull_request[5]
                    current_repo["open_pull_requests_to_master"].append(current_pull_request)
                    current_pull_request = {}
            returned_json.append(current_repo)
            current_repo = {}
        return json.dumps(returned_json)

    def pull_last_merged_pull_requests_commits(self, gaf_id):
        returned_json = []
        repos = repository.pull_repos_by_gaf_id(gaf_id)
        current_repo = {}
        for repo in repos:
            current_repo["repoid"] = repo[0]
            current_repo["reponame"] = repo[1]
            current_repo["commits"] = repository.pull_all_commits_of_last_merged_pull_request_by_repo_id(repo[0])
            current_repo["pull_request_date"] = \
                str((repository.pull_pull_request_date_by_id(current_repo["commits"][0][1]))[0][0])
            returned_json.append(current_repo)
            current_repo = {}
        return json.dumps(returned_json)

    def bamboo_last_activated_builds(self, gaf_id):
        returned_json = {"plans": []}
        plan_json = {}
        plans = repository.pull_plans_by_gaf(gaf_id)
        for plan in plans:
            plan_json["plankey"] = plan[0]
            plan_json["planname"] = plan[1]
            plan_json["buildsresults"] = []
            build_json = {}
            builds = repository.pull_builds_by_plan_key(plan[0])
            for build in builds:
                build_json["buildid"] = build[0]
                build_json["buildstarttime"] = str(build[2])
                build_json["buildstate"] = build[3]
                build_json["faildtestcount"] = build[6]
                build_json["successfultestcount"] = build[5]
                build_json["buildername"] = build[4]
                plan_json["buildsresults"].append(build_json)
                build_json = {}
            returned_json["plans"].append(plan_json)
            plan_json = {}
        return json.dumps(returned_json)

    def plan_data(self, gaf_id):
        returned_json = {"services": []}
        current_plan = {}
        plans = repository.pull_plans_by_gaf(gaf_id)
        for plan in plans:
            current_plan["key"] = plan[0]
            current_plan["serviceName"] = plan[1]
            if repository.pull_last_build_status(plan[0])[0][0]:
                current_plan["result"] = "success"
            else:
                current_plan["result"] = "failed"
            returned_json["services"].append(current_plan)
            current_plan = {}
        return json.dumps(returned_json)

    def all_bug_count(self, gaf_id):
        returned_json = {"Highest": 0,
                         "Medium": 0,
                         "Lowest": 0}
        all_bugs = repository.pull_bugs_stories_by_gaf(gaf_id)
        for bug in all_bugs:
            if bug[10] == "Highest":
                returned_json["Highest"] += 1
            elif bug[10] == "Medium":
                returned_json["Medium"] += 1
            else:
                returned_json["Lowest"] += 1
        return returned_json

    def work_reports_information(self, gaf_id):
        returned_json = {'teams': [],
                         'last_work_days': []}
        week_work_days = {0: "שני",
                          1: "שלישי",
                          2: "רביעי",
                          3: "חמישי",
                          6: "ראשון"}
        work_reports = repository.work_report_by_gaf_id(gaf_id)
        teams = self.get_teams_by_active_work_reports(work_reports)
        current_date = date.today() - timedelta(1)
        last_week_date = current_date - timedelta(7)
        day_index = (current_date.weekday() + 1) % 7
        friday_date = current_date - timedelta(7 + day_index - 5)
        saturday_date = current_date - timedelta(7 + day_index - 6)
        team_json = {}
        for team in teams:
            team_json["teamid"] = team[0][0]
            team_json["teamname"] = team[0][1]
            team_json["team_members"] = []
            developers = repository.pull_team_members_by_team_id(team[0][0])
            team_hours = [0, 0, 0, 0, 0]
            developer_json = {}
            for developer in developers:
                developer_json["id"] = developer[0]
                developer_json["name"] = developer[1]
                developer_json["member_hours"] = [0, 0, 0, 0, 0]
                day_counter = 1
                work_day_counter = 0
                while current_date != last_week_date:
                    if current_date != saturday_date and current_date != friday_date:
                        single_day_work_reports = repository.\
                            pull_relevant_work_report_by_developer_id_and_report_date(developer[0], current_date)
                        full_day_work_report = 0
                        for single_day_work_report in single_day_work_reports:
                            full_day_work_report += single_day_work_report[3]
                        developer_json["member_hours"][work_day_counter] = full_day_work_report
                        team_hours[work_day_counter] += full_day_work_report
                        work_day_counter += 1
                        if len(returned_json["last_work_days"]) < 5:
                            returned_json["last_work_days"].append(week_work_days[current_date.weekday()])
                    day_counter += 1
                    current_date = date.today() - timedelta(day_counter)
                team_json["team_members"].append(developer_json)
                developer_json = {}
                current_date = date.today() - timedelta(1)
            team_json["team_hours"] = team_hours
            returned_json["teams"].append(team_json)
            team_json = {}
        return json.dumps(returned_json)

    def get_teams_by_active_work_reports(self, work_reports):
        all_developers = []
        for work_report in work_reports:
            all_developers.append(repository.get_developer_by_work_report_id_query(work_report[1]))
        all_teams = []
        for developer in all_developers:
            if developer:
                all_teams.append(repository.get_team_by_developer(developer[0][0]))
        teams = []
        for team in all_teams:
            if team not in teams:
                teams.append(team)
        return teams

    @staticmethod
    def count_status(issue_type_return, stories):
        for story in stories:
            if story[7] == "To Do":
                issue_type_return["To Do"] += 1
            elif story[7] == "In Progress":
                issue_type_return["In Progress"] += 1
            else:
                issue_type_return["Done"] += 1

    def pull_stories_data(self, gaf_id):
        returned_json = {"features": {
            "To Do": 0,
            "In Progress": 0,
            "Done": 0},
            "bugs": {
                "To Do": 0,
                "In Progress": 0,
                "Done": 0}}
        Service.count_status(returned_json["features"], repository.pull_features_stories_by_gaf(gaf_id))
        Service.count_status(returned_json["bugs"], repository.pull_bugs_stories_by_gaf(gaf_id))
        return json.dumps(returned_json)

    def pull_all_cycles_ececution_stastus_by_project_id(self, project_id):
        '''
        The function return all Cycles with specific project id ,
        and show a count of them execution test statuses :
        Example :
        {
          "project id - 10103": [
            {
              "cycle_id": 1,
              "name": "cycle_name",
              "statuses": {
                "FAIL": 2,
                "PASS": 17,
                "UNEXECUTED": 0
              }
            },
            ...
          ]
        }
        :param project_id: - parameter to use in the query
        :return: return json
        '''
        cycles = repository.pull_cycles_id_by_project_id(project_id)
        json_entry_name = "project id - " + str(project_id)
        returned_json = {json_entry_name: []}
        for cycle in cycles:
            cycle_status_json = {}
            cycle_status_json["cycle_id"] = cycle[0]
            cycle_status_json["name"] = str(cycle[1])
            statuses = repository.pull_all_status_execution_tests_status_by_cycle_id(cycle_status_json["cycle_id"])
            statuses_json = {}

            statuses_checklist = {'FAIL':False,'UNEXECUTED':False,'PASS':False}
            for status in statuses:
                status_category = status[0]
                for category in statuses_checklist:
                    if category == status_category:
                        statuses_checklist[category] = True
                        statuses_json[category] = status[1]

            for category in statuses_checklist:
                if statuses_checklist[category] == False:
                    statuses_json[category] = 0

            cycle_status_json["statuses"] = statuses_json
            returned_json[json_entry_name].append(cycle_status_json)

        return returned_json


if __name__ == '__main__':
    service = Service()
    # pprint.pprint(service.pull_open_pull_requests_commits(1))
