FROM python

EXPOSE 5000

COPY . .

RUN pip install -r requirements.txt

RUN python -m pip install --trusted-host nexus.owls.shlomke.xyz -i http://nexus.owls.shlomke.xyz/repository/unity/simple exporterutilities

ENV FLASK_APP=Controller/Controller.py

CMD flask run --host=0.0.0.0
