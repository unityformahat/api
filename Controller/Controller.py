from flask import *
from Service.Service import Service


app = Flask(__name__)

service = Service()

base_url = '/api'


@app.route(base_url + '/work_reports/<gaf_id>')
def work_reports(gaf_id):
    return service.work_reports_information(gaf_id)


@app.route(base_url + '/bug_count/<gaf_id>')
def bug_count(gaf_id):
    return service.all_bug_count(gaf_id)


@app.route(base_url + '/commits/<gaf_id>')
def commits_pull(gaf_id):
    return service.pull_last_merged_pull_requests_commits(gaf_id)


@app.route(base_url + '/critical_bugs/<gaf_id>')
def critical_bugs(gaf_id):
    return service.critical_bugs_pull(gaf_id)


@app.route(base_url + '/stories_status_data/<gaf_id>')
def stories_status_data(gaf_id):
    return service.pull_stories_data(gaf_id)


@app.route(base_url + '/sprint/<gaf_id>')
def sprint_data(gaf_id):
    return service.pull_sprint_data(gaf_id)


@app.route(base_url + '/hierarchy')
def hierarchy_data():
    return service.pull_hierarchy_data()


@app.route(base_url + '/opened_pull_requests/<gaf_id>')
def open_pull_requests(gaf_id):
    return service.pull_open_pull_requests(gaf_id)


@app.route(base_url + '/last_builds/<gaf_id>')
def builds(gaf_id):
    return service.bamboo_last_activated_builds(gaf_id)


@app.route(base_url + '/last_build_for_plan/<gaf_id>')
def last_build_for_plan(gaf_id):
    return service.plan_data(gaf_id)


@app.route(base_url + '/cycles_tests_status/<int:project_id>/')
def show_post(project_id):
    return service.pull_all_cycles_ececution_stastus_by_project_id(project_id)


# if __name__ == "__main__":
#     app.run(debug=True,host= '0.0.0.0')
#     print('flask server down .')
