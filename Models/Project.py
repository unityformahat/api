class Project:

    def __init__(self, project_id, name, jira_project_id, bamboo_id, bitbucket_id, gaf_id):
        self.project_id = project_id
        self.name = name
        self.jira_project_id = jira_project_id
        self.bamboo_id = bamboo_id
        self.bitbucket_id = bitbucket_id
        self.gaf_id = gaf_id