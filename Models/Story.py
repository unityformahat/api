class Story:
    def __init__(self, story_id, name, creation_time, reporter_id, project_id, epic_id, sprint_id, status, assign, issue_type, priority):
        self.story_id = story_id
        self.name = name
        self.creation_time = creation_time
        self.reporter_id = reporter_id
        self.project_id = project_id
        self.epic_id = epic_id
        self.sprint_id = sprint_id
        self.status = status
        self.assign = assign
        self.issue_type = issue_type
        self.priority = priority